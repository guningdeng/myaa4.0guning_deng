<?php

namespace Threepenny;

class Identity
{
    static $name;
    static $https;
    // if true, this stops JavaScript being able to access the session id.
    static $httpOnly;
    static $salt;
    static $httpsText;

    public static function startSession($name = 'StemEffect', $https = FALSE)
    {
        self::$name = $name;
        self::$https = $https;
        self::$salt = 'En mijn oom staat op de bergen, hali, halo.';
        // This stops JavaScript being able to access the session id.
        self::$httpOnly = true;
        self::$httpsText = self::$https ? 'Beveiligde' : 'Onbeveiligde';

        if (session_status() === PHP_SESSION_NONE) {
            // Forces sessions to only use cookies.
            if (ini_set('session.use_only_cookies', 1) === FALSE) {
                $message = 'Onmogelijk om een veilige sessie te starten.';
            } else {
                // Gets current cookies params.
                $cookieParams = session_get_cookie_params();
                // specifies the lifetime of the cookie in seconds which is sent to the browser.
                // The value 0 means "until the browser is closed." Defaults to 0.
                //echo $cookieParams["lifetime"];
                session_set_cookie_params($cookieParams["lifetime"],
                    $cookieParams["path"],
                    $cookieParams["domain"],
                    self::$https,
                    self::$httpOnly);
                // Sets the session name to the one set above.
                session_name(self::$name);
                // session_save_path('d:\bin\temp');
                 $_SESSION['generated'] = time();
                //self::regenerate();
                session_start();
                $message = self::$httpsText . ' session is gestart.';
            }
        } else {
            $message = self::$httpsText . ' ' . self::$name . ' sessie is al opgestart.';
        }
        return $message;
    }

    static function isSessionStarted()
    {
        if (session_status() === PHP_SESSION_NONE) {
            return false;
        }
        return true;
    }

    public static function makeTicket($value)
    {
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        // hash the password with the unique salt.
        // XSS protection as we might print this value
        // session stays for one week valid
        return hash('sha512', self::$salt . $value . $userAgent);
    }

    public static function isTicketSet()
    {
        if (isset($_SESSION['t'])) {
            return true;
        }
        return false;
    }

    public static function setTicket($value)
    {
        $_SESSION['t'] = self::makeTicket($value);
    }

    public static function isValidTicket($value)
    {
        if (isset($_SESSION['t'])) {
            $userAgent = $_SERVER['HTTP_USER_AGENT'];
            $ticket = $_SESSION['t'];
            // session stays for one week valid
            if ($ticket == hash('sha512', self::$salt . $value . $userAgent)) {
                return self::$httpsText . ' ' . self::$name . ' session ticket is gevalideerd.';
            }
        }
        return self::$httpsText . ' ' . self::$name . ' session ticket niet gevalideerd.';
    }

    public static function setPositiveInteger($key, $value)
    {
        // XSS protection as we might print this value
        $_SESSION[$key] = preg_replace("/[^0-9]+/", "", $value);
    }

    public static function setText($key, $value)
    {
        // XSS protection as we might print this value
        $_SESSION[$key] = preg_replace("/[^a-zA-Z0-9_ \-]+/", "", $value);
    }

    public static function set($key, $value)
    {
        if (isset($_SESSION)) {
            $_SESSION[$key] = $value;
        }
    }

    public static function get($key)
    {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }
        return FALSE;
    }

    public static function regenerate($deleteOldSession = true)
    {
        if (!isset($_SESSION['generated']) || $_SESSION['generated'] < (time() - 20)) {
            // true will delete old session
            session_regenerate_id($deleteOldSession);
            $_SESSION['generated'] = time();
            $message = self::$httpsText . ' ' . self::$name . ' session is regenereerd.';
        } else {
            $message = self::$httpsText . ' ' . self::$name . ' session is niet regenereerd.';
        }
        return $message;
    }

    public static function endSession()
    {
        // session is started in the constructor
        // Unset all session values
        $_SESSION = array();
        // get session parameters
        $params = session_get_cookie_params();
        // Delete the actual cookie.
        setcookie(session_name(), '', time() - 42000,
            $params['path'],
            $params['domain'],
            $params['secure'],
            $params['httponly']);
        // Destroy session
        session_destroy();
        return self::$httpsText . ' ' . self::$name . ' session is beëindingd.';
    }

    public static function isAuthenticated()
    {
        $isLoggedIn = false;
        $nameInput = self::get('Name');
        if (isset($nameInput)) {
            $user = CRUD::readOne('User', $nameInput, $byColumnName = 'Name', array('Id', 'Name', 'HashedPassword', 'PersonId'));
            if ($user) {
                if (self::isValidTicket($user['HashedPassword'])) {
                    $isLoggedIn = true;
                }
            }
        }
        return $isLoggedIn;
    }

    public static function isAuthorized($role)
    {
        if (self::isAuthenticated()) {
            return true;
        } else {
            return false;
        }
    }

    public static function login($userName, $password)
    {
        $nameInput = CRUD::cleanUp($userName);
        // echo $nameInput .'test';
        // password typed in by user
        $userPasswordInput = $password;
        $user = CRUD::readOne('User', $nameInput, $byColumnName = 'Name',
            array('Id', 'Name', 'HashedPassword', 'PersonId', 'Role'));
        // var_dump($user);
        // echo CRUD::getMessage();
        if ($user) {
            if (password_verify($userPasswordInput, $user['HashedPassword'])) {
                self::setTicket($user['HashedPassword']);
                self::setPositiveInteger('UserId', $user['Id']);
                self::setText('Name', $user['Name']);
                self::setText('Role', $user['Role']);
                self::setPositiveInteger('Id', $user['PersonId']);
                // lookup Person
                $person = CRUD::readOne('Person', $user['PersonId'], $byColumnName = 'Id',
                    array('Id', 'FirstName', 'LastName', 'ShortName'));
                //var_dump($person);
                self::setText('ShortName', $person['ShortName']);
                self::setText('FirstName', $person['FirstName']);
                self::setText('LastName', $person['LastName']);
                return "{$user['Name']} is aangemeld.";
            } else {
                return "Verkeerde gebruikersnaam of paswoord. {$user['Name']} is niet aangemeld." .
                    CRUD::getMessage();
            }
        } else {
            return "{$nameInput} is niet gekend en dus niet aangemeld.";
        }
    }

    public static function isInRole($role)
    {
        if (self::isAuthenticated()) {
            if (self::get('Role') === $role) {
                return true;
            }
        }
        return false;
    }

    public static function isInRoles($roles)
    {
        if (self::isAuthenticated()) {
            if (in_array(self::get('Role'), $roles, true)) {
                return true;
            }
        }
        return false;
    }
}

