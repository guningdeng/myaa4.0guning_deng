<?php

/**
 * Created by ModernWays
 * User: Jef Inghelbrecht
 * Date: 26/06/2021
 * Revision:
 * Time: 17:32
 */

namespace Threepenny;
class Stats
{
    public static function getUserIpAddress()
    {
        $ipAddress = '';
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            // check for shared ISP IP
            $ipAddress = $_SERVER['HTTP_CLIENT_IP'];
        } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            // check for IPs passing through proxy servers
            // check if multiple IP addresses are set and take the first one
            $ipAddressList = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            foreach ($ipAddressList as $ipAddressItem) {
                $ipAddress = $ipAddressItem;
                break;
            }
        } else if (!empty($_SERVER['HTTP_X_FORWARDED'])) {
            $ipAddress = $_SERVER['HTTP_X_FORWARDED'];
        } else if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])) {
            $ipAddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
        } else if (!empty($_SERVER['HTTP_FORWARDED_FOR'])) {
            $ipAddress = $_SERVER['HTTP_FORWARDED_FOR'];
        } else if (!empty($_SERVER['HTTP_FORWARDED'])) {
            $ipAddress = $_SERVER['HTTP_FORWARDED'];
        } else if (!empty($_SERVER['REMOTE_ADDR'])) {
            $ipAddress = $_SERVER['REMOTE_ADDR'];
        }
        return $ipAddress;
    }

    public static function isValidIpAddress($ipAddress)
    {
        if (filter_var($ipAddress, FILTER_VALIDATE_IP,
                FILTER_FLAG_IPV4 | FILTER_FLAG_IPV6 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) === false) {
            return false;
        }
        return true;
    }

    public static function getLocation($ipAddress)
    {
        // https://askubuntu.com/questions/1160507/curl-not-working-in-the-latest-version-of-php-7-2
        $ch = curl_init('http://ipwhois.app/json/' . $ipAddress);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($ch);
        curl_close($ch);
        // Decode JSON response
        $ipWhoIsResponse = json_decode($json, true);
        // Country code output, field "country_code"
        return $ipWhoIsResponse;
    }

    public static function isFirstTimeVisit()
    {
        if (!isset($_SESSION['hasVisited'])) {
            $_SESSION['hasVisited'] = "yes";
            return true;
        } else {
            return false;
        }

    }

    public static function getCounter($fileName)
    {
        // Check if a text file exists.
        // If not create one and initialize it to zero.
        if (!file_exists($fileName)) {
            $statisticsFileName = fopen($fileName, "w");
            fwrite($statisticsFileName, "0");
            fclose($statisticsFileName);
        }

        // Read the current value of our counter file
        $statisticsFileName = fopen($fileName, "r");
        $counterVal = fread($statisticsFileName, filesize($fileName));
        fclose($statisticsFileName);
        return $counterVal;
    }

    public static function setCounter($fileName)
    {
        // Check if a text file exists.
        // If not create one and initialize it to zero.
        if (!file_exists($fileName)) {
            $statisticsFileName = fopen($fileName, "w");
            fwrite($statisticsFileName, "0");
            fclose($statisticsFileName);
        }

        // Read the current value of our counter file
        $statisticsFileName = fopen($fileName, "r");
        $counterVal = self::getCounter($fileName);
        $counterVal++;
        $statisticsFileName = fopen($fileName, "w");
        fwrite($statisticsFileName, $counterVal);
        fclose($statisticsFileName);
        return $counterVal;
    }

    public static function updateUserIpLogInfo($fileName)
    {
        $timestamp = gmdate("Y-m-d H:i:s", $_SERVER['REQUEST_TIME']);
        $ipAddress = self::getUserIpAddress();
        if (self::isValidIpAddress($ipAddress)) {
            $geoLocationData = self::getLocation($ipAddress);
        } else {
            $geoLocationData = array('country' => 'unknown',
                'region' => 'unknown',
                'city' => 'unknown');
        }
        // var_dump($geoLocationData);
        $logVisitFileName = fopen($fileName, "a");
        fwrite($logVisitFileName, $ipAddress . ';' .
            $geoLocationData['country'] . ';' .
            $geoLocationData['region'] . ';' .
            $geoLocationData['city'] . ';' .
            $timestamp . "\n");
        fclose($logVisitFileName);
    }
}