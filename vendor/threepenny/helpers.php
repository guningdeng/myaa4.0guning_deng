<?php

namespace Threepenny;

class Helpers
{
    public static function cleanUp($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        //echo $data;
        return $data;
    }

    public static function makeSafe($file)
    {
        // Joomla 3.3.2 version from JFile::makeSafe($file)
        // Remove any trailing dots, as those aren't ever valid file names.
        $file = rtrim($file, '.');
        $file = self::replaceSpecialChar($file);
        $regex = array('#(\.){2,}#', '#[^A-Za-z0-9\.\_\- ]#', '#^\.#');
        $file = trim(preg_replace($regex, '', $file));
        // echo $file;
        return $file;
        //$fileName = preg_replace('/[^a-zA-Z0-9_.-]/', '', $title);
        /* The ^ at the beginning of the character set, surrounded by [], states to not-match the list of characters.
        Therefore, the line can be read as "replace the characters that are not the following". */
    }

    public static function replaceSpecialChar($text)
    {
        $utf8 = array(
            '/[áàâãªä]/u' => 'a',
            '/[ÁÀÂÃÄ]/u' => 'A',
            '/[ÍÌÎÏ]/u' => 'I',
            '/[íìîï]/u' => 'i',
            '/[éèêë]/u' => 'e',
            '/[ÉÈÊË]/u' => 'E',
            '/[óòôõºö]/u' => 'o',
            '/[ÓÒÔÕÖ]/u' => 'O',
            '/[úùûü]/u' => 'u',
            '/[ÚÙÛÜ]/u' => 'U',
            '/ç/' => 'c',
            '/Ç/' => 'C',
            '/ñ/' => 'n',
            '/Ñ/' => 'N',
            '/–/' => '-', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚]/u' => ' ', // Literally a single quote
            '/[“”«»„]/u' => ' ', // Double quote
            '/ /' => ' ', // nonbreaking space (equiv. to 0x160)
        );
        // add specific special chars for an app, example C# -> CSharp for ModernWays
        if (defined('SPECIAL_CHARS_IN_H1')) {
            $utf8 = array_merge(SPECIAL_CHARS_IN_H1, $utf8);
        }
        //var_dump($utf8);
        return preg_replace(array_keys($utf8), array_values($utf8), $text);
    }

    public static function readFolder($path, $fileExtensions = array('html'))
    {
        $folder = array();
        $path = ltrim($path, '/');

        // locate $path in the myap filesystem
        $myapPath = $_SERVER['DOCUMENT_ROOT'] . '/' . $path;
        if (is_dir($myapPath)) {
            $dit = new \DirectoryIterator($myapPath);
            foreach ($dit as $file) {
                if (in_array($file->getExtension(), $fileExtensions)) {
                    $folder[] = [
                        'type' => $file->getExtension(),
                        'value' => "{$path}/{$file->getFileName()}",
                        'caption' => $file->getBasename('.' . $file->getExtension()),
                        'folderName' => ltrim(rtrim($path, '/'), '/')
                    ];
                }
                $columns = array_column($folder, 'caption');
                $array_lowercase = array_map('strtolower', $columns);
                array_multisort($array_lowercase, SORT_ASC, $folder);
            }
        }
        return $folder;
    }
}